<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it" lang="it">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" charset="utf-8" />
		<title>SS Redentore | Ravenna<?=isset($pagetitle)?" - ".$pagetitle:""?></title>
		<script type="text/javascript" src="https://www.google.com/recaptcha/api.js"></script>
		<script type="text/javascript" src="<?=PATH?>contents/jquery.js?<?=time()?>"></script>
		<script type="text/javascript" src="<?=PATH?>contents/global.js?<?=time()?>"></script>
		<script type="text/javascript" src="<?=PATH?>contents/content.js?<?=time()?>"></script>
		<script type="text/javascript" src="<?=PATH?>contents/waterfall.js?<?=time()?>"></script>
		<script type="text/javascript" src="<?=PATH?>contents/swiper.js?<?=time()?>"></script>
		<link rel="stylesheet" href="<?=PATH?>contents/styles.css?<?=time()?>"/>
	</head>
	<body>
		<header id="navbar" <?=((!LOGGED_IN || $myrow["role"] == "customer") ? "class=\"cart\"" : "")?>>
			<div></div>
			<?php 
				if(LOGGED_IN){
			?>
				<button class="custom menu" aria-label="open menu">
					<div></div>
				</button>
			<?php
				}
			?>
			<a id="logo" href="<?=PATH?>">
				<img alt="Logo sito: SS. Redentore" src="<?=PATH?>contents/logossr.png"/>
			</a>
			<a href="<?=PATH?>account.php">
				<div class="userinfo">
					<?php
						if(LOGGED_IN){
							echo "Logout";
						}
					?>
				</div>
			</a>
		</header>
		<?php 
			if(LOGGED_IN){?>
				<div id="menucontainer">
					<nav id="menu">
						<header>
							<button class="custom close" aria-label="chiudi menu">
								<div></div>
							</button>
							MENU
						</header>
						<ul>
							<?php
								$menu = array();
								if(LOGGED_IN){
									$menu[PATH."index.php"] = "Home";
									if($myrow["role"] == "admin" && isset($_SESSION["security_check_ok"])){
										// admin
										$menu[PATH."events_to_approve.php"] = "Gestione eventi";
										$menu[PATH."users_list.php"] = "Gestione utenti";
									}elseif($myrow["role"] == "organizer"){
										// atleta
										$menu[PATH."events_list.php"] = "I tuoi tesseramenti";
									}
									$menu[PATH."logout.php"] = "Logout";
								}
								foreach($menu as $link => $text){
									echo "<li>";
										echo "<a href=\"".$link."\">".$text."</a>";
									echo "</li>";
								}
							?>
						</ul>
					</nav>
				</div>
			<?php
				}
			?>
			<main class="background_style">
			
