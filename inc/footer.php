				<div class="white_column_right"></div>
				<div class="red_column_right"></div>
			</div>
		</main>
		<div id="alertcontainer">
			<section>
				<h1></h1>
				<p></p>
				<button name="ok"></button>
				<button name="cancel"></button>
			</section>
		</div>
		<footer id="rights">
			<img class="logo" alt="Logo sito: SS. Redentore" src="<?=PATH?>contents/logossr_footer.png" />
			<p>
				Società Sportiva Redentore
				<br />
				&copy; 2020 Giada Amaducci, Cecilia Teodorani
			</p>
			<a href="https://www.instagram.com/ss.redentore/">
				<img class="social" src="<?=PATH?>contents/instagram_icon.png" alt="Instagram"/>
			</a>
			<a href="https://www.facebook.com/redentoreravenna">
				<img class="social" src="<?=PATH?>contents/facebook_icon.png" alt="Facebook"/>
			</a>
		</footer>
	</body>
<html>
