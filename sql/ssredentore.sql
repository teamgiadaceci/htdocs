--
-- Table structure for table users
--

CREATE TABLE `users` (
`id` int(11) NOT NULL,
`name` varchar(100) NOT NULL,
`surname` varchar(100) NOT NULL,
`birthdate` date NOT NULL,
`birth_city` varchar(100) NOT NULL,
`phone` int(12) NOT NULL
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

ALTER TABLE `users`
ADD PRIMARY KEY (`id`);

ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Table structure for table parents
--

CREATE TABLE `parents` (
`fiscal_code` varchar(11) NOT NULL,
`user_id` int(11) NOT NULL DEFAULT '0'
) DEFAULT CHARSET=utf8;

ALTER TABLE `parents`
ADD PRIMARY KEY (`fiscal_code`),
ADD KEY `parents_ibfk_1` (`user_id`);

ALTER TABLE `parents`
ADD CONSTRAINT `parents_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Table structure for table athletes
--

CREATE TABLE `athletes` (
`fiscal_code` varchar(11) NOT NULL,
`user_id` int(11) NOT NULL DEFAULT '0',
`associate_id` int(11) NOT NULL DEFAULT '0',
`parent_id` varchar(11) NOT NULL,
`email` varchar(100) NOT NULL DEFAULT '',
`password` varchar(32) NOT NULL DEFAULT '',
`gender` enum('Maschio','Femmina','Altro') NOT NULL,
`image` varchar(200) NOT NULL DEFAULT '',
`nationality` varchar(100) NOT NULL DEFAULT '',
`number_identity_card` varchar(100) NOT NULL DEFAULT '',
`expiration_date` date NOT NULL
) DEFAULT CHARSET=utf8;

ALTER TABLE `athletes`
ADD PRIMARY KEY (`fiscal_code`),
ADD KEY `athletes_ibfk_1` (`user_id`),
ADD KEY `athletes_ibfk_2` (`parent_id`);

ALTER TABLE `athletes`
ADD CONSTRAINT `athletes_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
ADD CONSTRAINT `athletes_ibfk_2` FOREIGN KEY (`parent_id`) REFERENCES `parents` (`fiscal_code`);

--
-- Table structure for table residences
--

CREATE TABLE `residences` (
`id` int(11) NOT NULL,
`address` varchar(200) NOT NULL,
`city` varchar(100) NOT NULL,
`country` varchar(2) NOT NULL,
`zipcode` varchar(5) NOT NULL DEFAULT ''''''
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

ALTER TABLE `residences`
ADD PRIMARY KEY (`id`);

ALTER TABLE `residences`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Table structure for table users_residences
--

CREATE TABLE `users_residences` (
`id` int(11) NOT NULL,
`user_id` int(11) NOT NULL DEFAULT '0',
`residence_id` int(11) NOT NULL DEFAULT '0',
`date` date NOT NULL
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

ALTER TABLE `users_residences`
ADD PRIMARY KEY (`id`),
ADD KEY `users_residences_ibfk_1` (`user_id`),
ADD KEY `users_residences_ibfk_2` (`residence_id`);

ALTER TABLE `users_residences`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `users_residences`
ADD CONSTRAINT `users_residences_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
ADD CONSTRAINT `users_residences_ibfk_2` FOREIGN KEY (`residence_id`) REFERENCES `residences` (`id`);