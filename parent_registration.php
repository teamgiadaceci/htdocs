<?php
require_once(__DIR__."/inc/core.php");

$pagetitle = "Registrazione Genitore";

if(LOGGED_IN){
	location(PATH."account.php");
}

if(checkpost("do")){
	switch($_POST["do"]){
		case "register":
			if(!checkpost("name")){
				$output["message"] = "Inserisci il tuo nome";
			}elseif(!checkpost("surname")){
				$output["message"] = "Inserisci il tuo cognome";
			}elseif(!checkpost("birthdate")){
				$output["message"] = "Inserisci la tua data di nascita";
			}else{
				$birthdate = explode("-", $_POST["birthdate"]);
				if(count($birthdate) != 3 || !checkdate($birthdate[1], $birthdate[2], $birthdate[0]) || strtotime($_POST["birthdate"]) >= time()){
					$output["message"] = "La data di nascita inserita non &egrave; valida";
				}elseif(!checkpost("birthdate_address")){
					$output["message"] = "Inserisci il tuo luogo di nascita";
				}elseif(!checkpost("address")){
					$output["message"] = "Inserisci il tuo indirizzo di residenza";
				}elseif(!checkpost("iscritti_codicefiscale")){
					$output["message"] = "Inserisci il tuo codice fiscale";
				}elseif(!checkpost("city")){
					$output["message"] = "Inserisci la tua citt&agrave; di residenza";
				}elseif(!checkpost("zipcode")){
					$output["message"] = "Inserisci il tuo codice postale (CAP)";
				}elseif(!is_numeric($_POST["zipcode"]) || strlen($_POST["zipcode"]) != 5){
					$output["message"] = "Il codice postale (CAP) inserito non &egrave; valido";
				}elseif(!checkpost("country")){
					$output["message"] = "Inserisci la tua provincia di residenza";
				}elseif(strlen($_POST["country"]) != 2){
					$output["message"] = "Inserisci la provincia di residenza su 2 lettere";
				}else{
					$confirmation_code = generate_random_code(20);
					query("INSERT INTO users (email, password, name, surname) VALUES ('".escape($_POST["email"])."', '".escape(password($_POST["email"], $_POST["password"]))."', '".escape($_POST["name"])."', '".escape($_POST["surname"])."')");
					$user_id = insert_id();
					query("INSERT INTO customers (user_id, birthdate, address, city, country, zipcode, registration_date, confirmation_code) VALUES ('".escape($user_id)."', '".escape($_POST["birthdate"])."', '".escape($_POST["address"])."', '".escape($_POST["city"])."', '".escape($_POST["country"])."', '".escape($_POST["zipcode"])."', CURDATE(), '".escape($confirmation_code)."')");
					$output["result"] = "success";
				}
			}
		break;
	}
	output();
}

include(__DIR__."/inc/header.php");
?>
<section id="parentregistrationsection">
	<h1>Registrazione Genitore</h1>
	<p>
		Visto che sei minorenne abbiamo bisogno dei dati di un genitore o di un tutore.
	</p>
	<p>
		Per completare la registrazione ti chiediamo quindi di compilare il seguente modulo!
	</p>	
	<form>
	<input type="text" id="parentregistrationsection_name" name="name" autofocus required />
        <label for="parentregistrationsection_name">Nome</label>

		<input type="text" id="parentregistrationsection_surname" name="surname" required />
		<label for="parentregistrationsection_surname">Cognome</label>
        
        <select name="gender" >
        <option value="male" selected="selected">Uomo</option>
        <option value="female">Donna </option>
        <option value="other">Altro </option>
        </select>
        <label for="gender">Sesso</label>

		<input type="date" id="parentregistrationsection_birthdate" name="birthdate" required />
		<label for="parentregistrationsection_birthdate">Data di Nascita</label>

		<input type="text" id="parentregistrationsection_birthdate_address" name="birthdate_address" required />
        <label for="parentregistrationsection_birthdate_address">Luogo di Nascita</label>

		<input type="text" id="parentregistrationsection_cf" pattern="^[a-zA-Z]{6}[0-9]{2}[a-zA-Z][0-9]{2}[a-zA-Z][0-9]{3}[a-zA-Z]$" name="cf" required />
		<label for="parentregistrationsection_cf">Codice Fiscale</label>

		<input type="text" id="parentregistrationsection_address" name="address" required />
        <label for="parentregistrationsection_address">Indirizzo di Residenza</label>

		<input type="text" id="parentregistrationsection_city" name="city" required />
		<label for="parentregistrationsection_city">Città</label>

		<input type="text" id="parentregistrationsection_zipcode" name="zipcode" required />
		<label for="parentregistrationsection_zipcode">CAP</label>

		<input type="text" id="parentregistrationsection_country" name="country" required />
		<label for="parentregistrationsection_country">Provincia</label>
        
        <input type="text" id="parentregistrationsection_nation" name="nation" required />
		<label for="parentregistrationsection_country">Nazione</label>

		<input type="tel" id="parentregistrationsection_phone" name="phone" autofocus required />
        <label for="parentregistrationsection_phone">Numero di Telefono</label>

		<input type="submit" name="register" value="Termina Registrazione" />
	</form>
</section>
<script>
	$("section#organizerregistrationsection form").on("submit", function(e){
		e.preventDefault();
		formPost("organizerregistrationsection", function(data){
			if(checkData(data)){
				openAlert({
					title: "Fatto",
					text: "La regitrazione è completata! Entra nel tuo account per iscriverti ad uno sport!",
					okbutton: {
						text: "Ok",
						onclick: function(){
							loc("<?=PATH?>account.php");
						},
						close: false
					}
				});
			}
		});
	});
</script>
<?php
include(__DIR__."/inc/footer.php");
?>
