<?php
require_once(__DIR__."/inc/core.php");

$pagetitle = "Registrazione Admin";

if(!LOGGED_IN){
    location(PATH."login.php");
}

if(checkpost("do")){
	switch($_POST["do"]){
		case "adminregistration":
			if(!checkpost("name")){
				$output["message"] = "Inserisci il nome del nuovo amministratore";
			}elseif(!checkpost("surname")){
				$output["message"] = "Inserisci il cognome del nuovo amministratore";
			}elseif(!checkpost("email")){
				$output["message"] = "Inserisci l'indirizzo email del nuovo amministratore";
			}elseif(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
				$output["message"] = "L'indirizzo email inserito non &egrave; valido";
			}elseif(!checkpost("security_code")){
				$output["message"] = "Inserisci il codice di sicurezza del nuovo amministratore";
			}elseif(!checkpost("password")){
				$output["message"] = "Inserisci la password del nuovo amministratore";
			}else{
				//se non esistono email e password nella tabella, si può continuare, altrimenti bisogna cambiarli
                if(num_rows(query("SELECT null FROM users WHERE email = '".escape($_POST["email"])."'")) != 0){
					$output["message"] = "Esiste gi&agrave; un utente con questo indirizzo email";
				} else{
					query("INSERT INTO users(email,password,name,surname)
						   VALUES ('".escape($_POST["email"])."','".escape(password($_POST["email"],
						   $_POST["password"]))."', '".escape($_POST["name"])."', '".escape($_POST["surname"])."')");
					$id_user=fetch(query("SELECT id FROM users WHERE email ='".escape($_POST["email"])."'"))["id"];
					query("INSERT INTO admins(user_id,security_code)
						   VALUES ($id_user,'".escape($_POST["security_code"])."')");
					$output["result"] = "success";
				}
			}
		break;
	}
	output();
}

include(__DIR__."/inc/header.php");
?>
<section id="registrationadminsection">
	<h1>Registrazione Admin</h1>
	<form>
		<input type="text" id="name" name="name" autofocus />
        <label for="name">Nome</label>

		<input type="text" id="surname" name="surname" />
		<label for="surname">Cognome</label>

		<input type="email" id="registrationemail" name="email" />
		<label for="registrationemail">Email</label>

        <input type="password" id="security_code" name="security_code" />
        <label for="security_code">Codice di Sicurezza</label>

		<input type="password" id="adminpassword" name="password" />
        <label for="adminpassword">Password</label>
		<br />
		<input type="submit" name="adminregistration" value="Continua" />
	</form>
</section>

<script>
	$("section#registrationadminsection form").on("submit", function(e){
		e.preventDefault();
		formPost("registrationadminsection", function(data){
			if(checkData(data)){
				openAlert({
					title: "Fatto",
					text: "La registrazione &egrave; completata con successo!",
					okbutton: {
						text: "Ok",
						onclick: function(){
							loc("<?=PATH?>account.php");
						},
						close: false
					}
				});
			}
		});
	});
</script>
<?php
include(__DIR__."/inc/footer.php");
?>
