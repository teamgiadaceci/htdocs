<?php
require_once(__DIR__."/inc/core.php");

$pagetitle = "Accedi";

if(LOGGED_IN){
	location(PATH."account.php");
}

if(checkpost("do")){
	switch($_POST["do"]){
		case "requestnewpassword":
			if(!checkpost("email")){
				$output["message"] = "Inserisci la tua email";
			}else{
				$user = user_info($_POST["email"], $_POST["password"]);
				if($user === false){
					$output["message"] = "Email o password errati";
				}elseif($user["confirmation_code"] != ""){
					$output["message"] = "Benvenuto <span class='important'>".entities($user["name"])."</span>, il tuo account non &egrave; ancora confermato. Controlla la tua email e clicca sul pulsante che trovi al suo interno.<br/><br/>Ci vediamo tra poco!";
				}else{
					$_SESSION["email"] = $_POST["email"];
					$_SESSION["password"] = $_POST["password"];
					$output["result"] = "success";
				}
			}
		break;
	}
	output();
}

include(__DIR__."/inc/header.php");
?>
<section id="loginsection" class="min">
	<h1>Accedi</h1>
	<form>
		<input type="email" id="loginemail" name="email" <?=checkget("email")?"value=\"".$_GET["email"]."\"":"autofocus"?> />
		<label for="loginemail">Email</label>
		<input type="password" id="loginpassword" name="password" <?=checkget("email")?"autofocus":""?> />
		<label for="loginpassword">Password</label>
		<input type="submit" name="requestnewpassword" value="Accedi" />
	</form>
	<a href="<?=PATH?>login.php">
		<button class="empty">Torna all'accesso</button>
	</a>
</section>
<script src='https://www.google.com/recaptcha/api.js?hl=it'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<script>
	$("section#loginsection form").on("submit", function(e){
		e.preventDefault();
		formPost("loginsection", function(data){
			if(checkData(data)){
				reload();
			}
		});
	});
</script>
<?php
include(__DIR__."/inc/footer.php");
?>
