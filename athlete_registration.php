<?php
require_once(__DIR__."/inc/core.php");

$pagetitle = "Registrazione Atleta";

if(LOGGED_IN){
	location(PATH."account.php");
}

if(checkpost("do")){
	switch($_POST["do"]){
		case "register":
			if(!checkpost("name")){
				$output["message"] = "Inserisci il tuo nome";
			}elseif(!checkpost("surname")){
				$output["message"] = "Inserisci il tuo cognome";
			}elseif(!checkpost("birthdate")){
				$output["message"] = "Inserisci la tua data di nascita";
			}else{
				$birthdate = explode("-", $_POST["birthdate"]);
				if(count($birthdate) != 3 || !checkdate($birthdate[1], $birthdate[2], $birthdate[0]) || strtotime($_POST["birthdate"]) >= time()){
					$output["message"] = "La data di nascita inserita non &egrave; valida";
				}elseif(!checkpost("birthdate_address")){
					$output["message"] = "Inserisci il tuo luogo di nascita";
				}elseif(!checkpost("address")){
					$output["message"] = "Inserisci il tuo indirizzo di residenza";
				}elseif(!checkpost("iscritti_codicefiscale")){
					$output["message"] = "Inserisci il tuo codice fiscale";
				}elseif(!checkpost("city")){
					$output["message"] = "Inserisci la tua citt&agrave; di residenza";
				}elseif(!checkpost("zipcode")){
					$output["message"] = "Inserisci il tuo codice postale (CAP)";
				}elseif(!is_numeric($_POST["zipcode"]) || strlen($_POST["zipcode"]) != 5){
					$output["message"] = "Il codice postale (CAP) inserito non &egrave; valido";
				}elseif(!checkpost("country")){
					$output["message"] = "Inserisci la tua provincia di residenza";
				}elseif(strlen($_POST["country"]) != 2){
					$output["message"] = "Inserisci la provincia di residenza su 2 lettere";
				}elseif(!checkpost("email")){
					$output["message"] = "Inserisci il tuo indirizzo email";
				}elseif(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
					$output["message"] = "L'indirizzo email inserito non &egrave; valido";
				}elseif(num_rows(query("SELECT null FROM users WHERE email = '".escape($_POST["email"])."'")) != 0){
					$output["message"] = "Esiste gi&agrave; un utente con questo indirizzo email";
				}elseif(!checkpost("password")){
					$output["message"] = "Scegli una password";
				}elseif(strlen($_POST["password"]) < 6){
					$output["message"] = "La password deve essere lunga almeno 6 caratteri";
				}elseif(!checkpost("repeatpassword") || $_POST["password"] != $_POST["repeatpassword"]){
					$output["message"] = "Le due password inserite non coincidono";
				}elseif(!checkpost("privacy") || $_POST["privacy"] != 1){
					$output["message"] = "Devi accettare l'informativa sulla privacy";
				}else{
					$confirmation_code = generate_random_code(20);
					query("INSERT INTO users (email, password, name, surname) VALUES ('".escape($_POST["email"])."', '".escape(password($_POST["email"], $_POST["password"]))."', '".escape($_POST["name"])."', '".escape($_POST["surname"])."')");
					$user_id = insert_id();
					query("INSERT INTO athletes (user_id, birthdate, address, city, country, zipcode, registration_date, confirmation_code) VALUES ('".escape($user_id)."', '".escape($_POST["birthdate"])."', '".escape($_POST["address"])."', '".escape($_POST["city"])."', '".escape($_POST["country"])."', '".escape($_POST["zipcode"])."', CURDATE(), '".escape($confirmation_code)."')");
					if(strtotime($_POST["birthdate"]) >= strtotime("18 years ago")){
						$output["message"] = "minorenne";
					}else{
						$output["message"] = "maggiorenne";
					}
					$output["result"] = "success";
				}
			}
		break;
	}
	output();
}

include(__DIR__."/inc/header.php");
?>
<section id="athleteregistrationsection">
	<h1>Registrazione Atleta</h1>
	<form> 
		<input type="text" id="athleteregistrationsection_name" name="name" autofocus />
        <label for="athleteregistrationsection_name">Nome</label>

		<input type="text" id="athleteregistrationsection_surname" name="surname" autofocus />
		<label for="athleteregistrationsection_surname">Cognome</label>
        
        <select name="gender">
        <option value="male">Maschio</option>
        <option value="female" selected="selected">Femmina</option>
        <option value="other">Altro</option>
        </select>
        <label for="gender">Sesso</label>

		<input type="date" id="athleteregistrationsection_birthdate" name="birthdate" autofocus />
		<label for="athleteregistrationsection_birthdate">Data di Nascita</label>

		<input type="text" id="athleteregistrationsection_birthdate_address" name="birthdate_address" autofocus  />
        <label for="athleteregistrationsection_birthdate_address">Luogo di Nascita</label>

		<input type="text" id="athlete_cf" pattern="^[a-zA-Z]{6}[0-9]{2}[a-zA-Z][0-9]{2}[a-zA-Z][0-9]{3}[a-zA-Z]$" name="cf" autofocus   />
		<label for="athlete_cf">Codice Fiscale</label>
        
        <input type="text" id="athlete_identitycard" name="identity_card" autofocus  />
        <label for="athlete_identitycard">Numero Carta Identità</label>
        
        <input type="date" id="athlete_identitycard_expiration_date" name="expiration_date" autofocus   />
        <label for="athlete_identitycard_expiration_date">Data Scadenza Carta Identità</label>

		<input type="text" id="athleteregistrationsection_address" name="address" placeholder="Via Enrico Fermi, 10" autofocus   />
        <label for="athleteregistrationsection_address">Indirizzo di Residenza</label>

		<input type="text" id="athleteregistrationsection_city" name="city" autofocus   />
		<label for="athleteregistrationsection_city">Città</label>

		<input type="text" id="athleteregistrationsection_zipcode" name="zipcode" autofocus   />
		<label for="athleteregistrationsection_zipcode">CAP</label>

		<input type="text" id="athleteregistrationsection_country" name="country" autofocus   />
		<label for="athleteregistrationsection_country">Provincia</label>
        
        <input type="text" id="athleteregistrationsection_nationality" name="nationality" autofocus   />
		<label for="athleteregistrationsection_nationality">Nazionalità</label>
        
        <input type="tel" id="athleteregistrationsection_phone" name="phone" placeholder="" autofocus   />
        <label for="athleteregistrationsection_phone">Numero di Telefono</label>

		<input type="email" id="athleteregistrationsection_email" name="email" autofocus   />
		<label for="athleteregistrationsection_email">Email</label>

		<input type="password" id="athleteregistrationsection_password" name="password"   />
        <label for="athleteregistrationsection_password">Password</label>

		<input type="password" id="athleteregistrationsection_repeatpassword" name="repeatpassword"   />
		<label for="athleteregistrationsection_repeatpassword">Ripeti Password</label>
        
        <select name="sport" >
        <option value="soccer">Calcio</option>
        <option value="tchoukball" selected="selected">Tchoukball</option>
        <option value="volleyball">Pallavolo</option>
        </select>
        <label for="sport">Sport</label>
		
		<br></br>

		<div class="privacybox">
			<div class="title main">INFORMATIVA PRIVACY AI SENSI DELL'ART. 13 REG. 679/2016 E NORME COLLEGATE</div>
			<br/>
			Ai sensi dell'art. 13 del Reg. 679/2016 (Regolamento per la protezione dei dati personali, detto anche GDPR") e norme collegate, con riferimento ai suoi dati personali e/o a quelli del minore (d'ora in avanti detti l'Interessato - cioè la persona fisica a cui si riferiscono i dati personali raccolti e trattati) su cui esercita la responsabilità genitoriale di cui i responsabili del trattamento dei dati entreranno in possesso, desideriamo informarla di quanto segue:<br/>
			<br/>
			<div class="title">1. Finalità di trattamento dei dati </div>
			I dati personali da lei forniti saranno trattati per le seguenti finalità:<br/>
			a) riconoscimento dell'utente all'interno della piattaforma attraverso un account;<br/>
			b) inserimento dei dati anagrafici richiesti nei database informatici;<br/>
			c) tenuta della documentazione e svolgimento della gestione contabile, incassi e pagamenti;<br/>
			d) adempimento di tutti gli obblighi di legge connessi allo svolgimento e all'organizzazione dell'attività della piattaforma;<br/>
			e) promozione dell'attività della piattaforma;<br/>
			f) elaborazione di studi, ricerche statistiche e di mercato, profili utenti a scopi commerciali e di marketing;<br/>
			g) invio di materiale pubblicitario ed informativo;<br/>
			h) cessione o concessione a terzi dei dati personali anche a scopo di lucro o per finalità commerciali.<br/>
			<br/>
			<div class="title">2. Modalità di trattamento dei dati </div>
			2.1 Il trattamento dei dati personali avviene mediante elaborazioni manuali o strumenti automatizzati per tutto il tempo in cui la piattaforma sarà attiva e/o l'account sarà attivo. Specifiche misure di sicurezza sono osservate per prevenire la perdita di dati, usi illeciti o non corretti ed accessi non autorizzati.<br/>
			2.2 Il trattamento di dati personali di terzi, forniti dall'Interessato, sarà possibile per le finalità suindicate; l'Interessato avrà cura di fornire al terzo l'informativa relativa al presente trattamento, raccogliendo la sua autorizzazione al trattamento se dovuta.<br/>
			<br/>
			<div class="title">3. Natura dei dati </div>
			I dati trattati dall'associazione saranno tutti quelli richiesti dalla stessa per iscritto (a titolo esemplificativo: dati anagrafici, contatti, indirizzi telematici ecc.), o comunque conferiti nel corso dell'utilizzo della piattaforma.<br/>
			<br/>
			<div class="title">4. Diffusione dei dati personali </div>
			I dati personali forniti potranno essere oggetto di diffusione esclusivamente agli organizzatori di eventi che, per tutela della compravendita, devono essere a conoscenza di chi ne acquista o prenota gratuitamente i biglietti.<br/>
			<br/>
			<div class="title">5. Comunicazione dei dati </div>
			Al trattamento dei dati personali saranno applicate tutte le misure di sicurezza, tecniche ed organizzative, ritenute idonee dal Titolare ai sensi dell'art. 32 GDPR. I dati non saranno oggetto di trasferimento al di fuori del territorio dell'Unione Europea, se non previa informativa all'Interessato e con illustrazione dell'adeguatezza oppure delle garanzie appropriate o delle deroghe alle stesse applicabili, ex artt. 42-50 GDPR.<br/>
			I suoi dati personali non verranno comunicati, ceduti o licenziati a terzi per finalità commerciale.<br/>
			<br/>
			<div class="title">6. Dati di terzi </div>
			La responsabilità relativa ai dati personali di soggetti terzi, che venissero dalla piattaforma trattati in ogni modo a seguito di comunicazione e/o diffusione da parte sua, sarà imputabile esclusivamente a Lei, che garantisce di avere il diritto di comunicarli e diffonderli e libera il titolare del trattamento nei confronti dei terzi interessati.<br/>
			I dati di terzi di cui Lei viene in possesso, sono da considerarsi utilizzabili esclusivamente per l'utilizzo nella circoscritta situazione di gestione dell'evento e di analisi dei partecipanti ad esso.<br/>
			<br/>
			<div class="title">7. Diritti dell'interessato </div>
			7.1 I diritti spettanti all'Interessato sono i seguenti ex artt. 7, 13-22 GDPR, per quanto applicabili al singolo caso concreto:
			a) ha il diritto di chiedere al Titolare del trattamento l'accesso ai suoi dati personali, chiedendo conferma o meno della loro esistenza nonché la rettifica o la cancellazione degli stessi o la limitazione (blocco temporaneo) del trattamento che lo riguarda;<br/>
			b) se ha fornito il consenso per una o più specifiche finalità, ha il diritto di revocare tale consenso in qualsiasi momento;<br/>
			c) ha il diritto di proporre reclamo alla seguente Autorità di Controllo: Garante per la protezione dei dati personali (<a href="http://www.garanteprivacy.it" target="_blank">http://www.garanteprivacy.it</a>); ha comunque facoltà di proporre alternativamente reclamo all'autorità competente dello Stato membro ove risiede abitualmente, lavora o del luogo ove si è verificata la presunta violazione;<br/>
			d) ha il diritto alla portabilità dei suoi dati personali (per quelli con base legale di esecuzione contrattuale o consensuale) mediante richiesta al Titolare, a mezzo di comunicazione di un file in formato .csv, .xml o analogo;<br/>
			e) ha il diritto di OPPORSI in qualsiasi momento al loro trattamento per motivi connessi alla sua situazione particolare in caso di trattamento necessario per l'esecuzione di un compito di interesse pubblico o connesso all'esercizio di pubblici poteri, oppure in caso di perseguimento di legittimo interesse del Titolare<br/>
			7.2. Il trattamento avviene mediante processi automatizzati che non determinano la profilazione degli Interessati.<br/>
			7.3. Per esercitare i suddetti diritti l'Interessato può contattare i titolari al recapito contrattuale.<br/>
			<br/>
			<div class="title">8. Titolare del Trattamento </div>
			Titolari del trattamento per l'esercizio dei suoi diritti sono i seguenti:<br/>
			Giada Amaducci, Marco Desiderio, Cecilia Teodorani<br/><a href="mailto:outsidevents@gmail.com">outsidevents@gmail.com</a><br/>
			<br/>
			Per ogni chiarimento terminologico, si veda la guida del Garante: <a href="https://www.garanteprivacy.it/regolamentoue" target="_blank">https://www.garanteprivacy.it/regolamentoue</a>.
		</div>

		<label for="athleteregistrationsection_privacy">Accetto l'informativa sulla privacy</label>
		<input type="checkbox" id="athleteregistrationsection_privacy" name="privacy" />

		<input type="submit" name="register" value="Registrati" />
	</form>
</section>

<script>
	$("section#athleteregistrationsection form").on("submit", function(e){
		e.preventDefault();
		formPost("athleteregistrationsection", function(data){
			if(checkData(data)){
				if(data["message"] == "minorenne"){
					openAlert({
						title: "Fatto",
						text: "La registrazione &egrave; completata con successo! Essendo minorenne richiediamo anche l'inserimento dei dati di un genitore.",
						okbutton: {
							text: "Ok",
							onclick: function(data){
								loc("<?=PATH?>parent_registration.php");
							},
							close: false
						}
					});
				}else{
					openAlert({
						title: "Fatto",
						text: "La registrazione &egrave; completata con successo! Prima di effettuare l'accesso, controlla la tua email per confermare il tuo account.",
						okbutton: {
							text: "Ok",
							onclick: function(){
								loc("<?=PATH?>index.php");
							},
							close: false
						}
					});
				}
				
			}
		});
	});
</script>

<?php
include(__DIR__."/inc/footer.php");
?>
