<?php
require_once(__DIR__."/inc/core.php");

$pagetitle = "Accedi";

if(LOGGED_IN){
	location(PATH."account.php");
}

if(checkpost("do")){
	switch($_POST["do"]){
		case "login":
			if(!checkpost("email") || !checkpost("password")){
				$output["message"] = "Email o password errati";
			}else{
				$user = user_info($_POST["email"], $_POST["password"]);
				if($user === false){
					$output["message"] = "Email o password errati";
				}elseif($user["role"] == "customer" && $user["confirmation_code"] != ""){
					$output["message"] = "Benvenuto <span class='important'>".entities($user["name"])."</span>, il tuo account non &egrave; ancora confermato. Controlla la tua email e clicca sul pulsante che trovi al suo interno.<br/><br/>Ci vediamo tra poco!";
				}else{
					$_SESSION["email"] = $_POST["email"];
					$_SESSION["password"] = $_POST["password"];
					$output["result"] = "success";
				}
			}
		break;
	}
	output();
}

include(__DIR__."/inc/header.php");
?>
<section id="loginsection" class="min">
	<h1>Benvenuto/a!</h1>
	<form>
		<input type="email" id="loginemail" name="email" <?=checkget("email")?"value=\"".$_GET["email"]."\"":"autofocus"?> />
		<label for="loginemail">Email</label>
		<input type="password" id="loginpassword" name="password" <?=checkget("email")?"autofocus":""?> />
		<label for="loginpassword">Password</label>
		<input type="submit" name="login" value="Accedi" />
	</form>
	<a href="<?=PATH?>athlete_registration.php">
		<button class="empty">Registrati</button>
	</a>
</section>
<script>
	$("section#loginsection form").on("submit", function(e){
		e.preventDefault();
		formPost("loginsection", function(data){
			if(checkData(data)){
				<?php if(isset($_SESSION["cart"]) && count($_SESSION["cart"]) > 0){ ?>
					loc("<?=PATH?>cart.php");
				<?php }else{ ?>
					reload();
				<?php } ?>
			}
		});
	});
</script>
<?php
include(__DIR__."/inc/footer.php");
?>
