<?php
define("PAGE_SECURITY_CHECK", true);
require_once(__DIR__."/inc/core.php");

$pagetitle = "Controllo di Sicurezza";

if(!LOGGED_IN || $myrow["role"] != "admin" || isset($_SESSION["security_check_ok"])){
	location(PATH);
}

if(isset($_POST["do"])){
	switch($_POST["do"]){
		case "check":
			if(!checkpost("security_code") || $_POST["security_code"] != $myrow["security_code"]){
				$output["message"] = "Il codice inserito non è corretto";
			}else{
				$_SESSION["security_check_ok"] = true;
				$output["result"] = "success";
			}
			break;
	}
	output();
}

include(__DIR__."/inc/header.php");
?>
<section id="securitychecksection" class="min">
	<h1>Bentornato <?=entities($myrow["name"])?></h1>
	<p class="center">
		Verifica la tua identità
		<form>
			<input type="password" id="security_code" name="security_code" autofocus />
			<label for="security_code">Codice di sicurezza</label>
			<input type="submit" name="check" value="Conferma" />
		</form>
		<a href="<?=PATH?>logout.php">
			<button class="empty">Disconnettiti</button>
		</a>
	</p>
</section>
<script>
$("section#securitychecksection form").on("submit", function(e){
	e.preventDefault();
	formPost("securitychecksection", function(data){
		if(data["result"] == "success"){
			reload();
		}else{
			openAlert({
				title: "Whoops..",
				text: data["message"],
				okbutton: {
					text: "Ok"
				}
			});
		}
	});
});
</script>
<?php
include(__DIR__."/inc/footer.php");
?>
