<?php
require_once(__DIR__."/inc/core.php");

if($myrow["role"] != "admin" || !checkget("id")){
    location(PATH);
}

if(!checkget("id")){
    location(PATH);
}
$user_to_search = query("SELECT u.*,
                (SELECT COUNT(*) FROM admins WHERE user_id = u.id) as is_admin,
                (SELECT COUNT(*) FROM customers WHERE user_id = u.id) as is_customer,
                (SELECT COUNT(*) FROM organizers WHERE user_id = u.id) as is_organizer
                FROM users u
                WHERE u.id = '".escape($_GET["id"])."'");
if(num_rows($user_to_search) != 1){
    location(PATH);
}else{
    $user_to_search = fetch($user_to_search);
    if($user_to_search["is_admin"]){
        $user_to_search["role"] = "admin";
        $pagetitle = "Dati Admin";
    }elseif($user_to_search["is_organizer"]){
        $user_to_search["role"] = "organizer";
        $pagetitle = "Dati Organizzatore";
    }elseif($user_to_search["is_customer"]){
        $user_to_search["role"] = "customer";
        $pagetitle = "Dati Cliente";
    }
}

if(checkpost("do")){
    switch($_POST["do"]){
        case "approveorganizer":
            query("UPDATE organizers
                    SET approved = '1'
                    WHERE user_id = '".escape($user_to_search["id"])."'");
            $pasw = generate_random_code();
            query("UPDATE users
                    SET password = '".escape(password($user_to_search["email"], $pasw))."'
                    WHERE id = '".escape($user_to_search["id"])."'");
            send_email($user_to_search["email"], "Approvazione account", entities($user_to_search["name"]).", ti diamo il benvenuto tra i nostri organizzatori di <b>Outside</b>!<br><br>Siamo molto felici di averti nella nostra squadra. <br /><br /> La password per entrare nel tuo nuovo profilo &egrave;: ".$pasw.", ti consigliamo di modificarla nel tuo primo accesso.<br /><br />Da questo momento in poi potrai pubblicare i tuoi eventi.<br /><br /><a href=\"".PATH."login.php?email=".$user_to_search["email"]."\" style=\"display: inline-block; border: 2px solid #56157E; padding: 5px 20px; color: #fff; background-color: #6a1b9a; border-radius: 5px; text-decoration: none; font-weight: bold;\">CLICCA QUI PER EFFETTUARE L'ACCESSO</a><br /><br />A presto!");
            $output["result"] = "success";
            break;
    }
    output();
}

include(__DIR__."/inc/header.php");
?>

<?php
    switch($user_to_search["role"]){
        case "organizer":
            $user = fetch(query("SELECT u.name, u.surname, u.email, o.phone_number, o.type, o.business_name,
            o.id_number, o.website, o.registration_date, o.approved
            FROM users u, organizers o WHERE u.id = '".escape($_GET["id"])."' AND u.id = o.user_id"));
            ?>
            <a class="fullbuttoncontainer" href="<?=PATH?>users_list.php">
	            <button>Torna all'elenco degli Utenti</button>
            </a>
            <section id="organizerdata" class="margintop">
                <h1>Dati dell'Organizzatore</h1>
                <form>
                    <input type="text" id="organizerdata_name" name="name" value="<?=$user["name"]?>" disabled />
                    <label for="organizerdata_name">Nome</label>
                    <input type="text" id="organizerdata_surname" name="surname" value="<?=$user["surname"]?>" disabled />
                    <label for="organizerdata_surname">Cognome</label>
                    <input type="email" id="organizerdata_registrationemail" name="email" value="<?=$user["email"]?>" disabled />
                    <label for="organizerdata_registrationemail">Email</label>
                    <?php
                        if($user["type"]=="company"){
                        ?>
                            <input type="text" id="organizerdata_type" name="type" value="Azienda" disabled />
                            <label for="organizerdata_type">Tipo Azienda</label>
                            <input type="text" id="organizerdata_businessname" name="businessname" value="<?=$user["business_name"]?>" disabled/>
                            <label for="organizerdata_businessname">Nome Azienda</label>
                            <input type="text" id="organizerdata_idnumber" name="idnumber" value="<?=$user["id_number"]?>" disabled/>
                            <label for="organizerdata_idnumber">Partita Iva</label>
                    <?php
                        }else{
                        ?>
                            <input type="text" id="organizerdata_type" name="type" value="Privato" disabled />
                            <label for="organizerdata_type">Tipo di Azienda</label>
                            <input type="text" id="organizerdata_idnumber" name="idnumber" value="<?=$user["id_number"]?>" disabled/>
                            <label for="organizerdata_idnumber">Codice Fiscale</label>
                        <?php
                        }
                    ?>
                    <input type="tel" id="organizerdata_phonenumber" name="phonenumber" value="<?=$user["phone_number"]?>" disabled />
                    <label for="organizerdata_phonenumber">Numero di Telefono</label>
                    <input type="url" id="organizerdata_website" name="website" value="<?=$user["website"]?>" disabled/>
                    <label for="organizerdata_website">Sito Internet</label>
                    <input type="date" id="organizerdata_registrationdate" name="registrationdate" value="<?=$user["registration_date"]?>" disabled />
                    <label for="organizerdata_registrationdate">Data di Registrazione</label>
                <?php
                    if($user["approved"]=='0'){?>
                        <input type="submit" name="approveorganizer" value="Approva Organizzatore" />
                <?php
                    }?>
                </form>
            </section>
            <script type="text/javascript">
                $("section#organizerdata form").on("submit", function(e){
                    e.preventDefault();
                    formPost("organizerdata", function(data){
                        if(checkData(data)){
                            openAlert({
                                title: "Fatto",
                                text: "L'organizzatore è stato approvato",
                                okbutton: {
                                    text: "Ok",
                                    onclick: function(){
                                        loc("<?=PATH?>users_list.php");
                                    },
                                    close: false
                                }
                            });
                        }
                    });
			});
            </script>
            <?php
            break;
        case "customer":
            $user = fetch(query("SELECT u.name, u.surname, u.email, c.birthdate, c.address, c.city,
            c.zipcode, c.country, c.registration_date
            FROM users u, customers c WHERE u.id='".escape($_GET["id"])."' AND u.id=c.user_id"));
            ?>
            <a class="fullbuttoncontainer" href="<?=PATH?>users_list.php">
	            <button>Torna all'elenco degli Utenti</button>
            </a>
            <section id="customerdata" class="margintop">
                <h1>Dati del Cliente</h1>
                <form>
                    <input type="text" id="customerdata_name" name="name" value="<?=$user["name"]?>" disabled />
                    <label for="customerdata_name">Nome</label>
                    <input type="text" id="customerdata_surname" name="surname" value="<?=$user["surname"]?>" disabled />
                    <label for="customerdata_surname">Cognome</label>
                    <input type="email" id="customerdata_registrationemail" name="email" value="<?=$user["email"]?>" disabled />
                    <label for="customerdata_registrationemail">Email</label>
                    <input type="date" id="customerdata_birthdate" name="birthdate" value="<?=$user["birthdate"]?>" disabled />
                    <label for="customerdata_birthdate">Data di nascita</label>
                    <input type="text" id="customerdata_address" name="address" value="<?=$user["address"]?>" disabled />
                    <label for="customerdata_address">Indirizzo di residenza</label>
                    <input type="text" id="customerdata_city" name="city" value="<?=$user["city"]?>" disabled />
                    <label for="customerdata_city">Città</label>
                    <input type="text" id="customerdata_zipcode" name="zipcode" value="<?=$user["zipcode"]?>" disabled />
                    <label for="customerdata_zipcode">CAP</label>
                    <input type="text" id="customerdata_country" name="country" value="<?=$user["country"]?>" disabled />
                    <label for="customerdata_country">Provincia</label>
                    <input type="date" id="customerdata_registrationdate" name="registrationdate" value="<?=$user["registration_date"]?>" disabled />
                    <label for="customerdata_registrationdate">Data di Registrazione</label>
                </form>
            </section>
            <?php
        break;
        case "admin":
            $user = fetch(query("SELECT u.name, u.surname, u.email FROM users u WHERE u.id = '".escape($_GET["id"])."'"));
            ?>
            <a class="fullbuttoncontainer" href="<?=PATH?>users_list.php">
	            <button>Torna all'elenco degli Utenti</button>
            </a>
            <section id="admindata" class="margintop">
                <h1>Dati dell'Admin</h1>
                <form>
                    <input type="text" id="admindataname" name="name" value="<?=$user["name"]?>" disabled />
                    <label for="admindataname">Nome</label>
                    <input type="text" id="admindatasurname" name="surname" value="<?=$user["surname"]?>" disabled />
                    <label for="admindatasurname">Cognome</label>
                    <input type="email" id="admindataemail" name="email" value="<?=$user["email"]?>" disabled />
                    <label for="admindataemail">Email</label>
                </form>
            </section>
            <?php
        break;
    case "customer":
        $user = fetch(query("SELECT u.name, u.surname, u.email, c.birthdate, c.address, c.city,
        c.zipcode, c.country, DATE_FORMAT('%d-%m-%Y', c.registration_date) as registration_date
        FROM users u, customers c WHERE u.id = '".escape($_GET["id"])."' AND u.id = c.user_id"));
        ?>
        <section id="customerdata" class="margintop">
            <h1>Dati del Cliente</h1>
            <form>
                <input type="text" id="customerdata_name" name="name" value="<?=$user["name"]?>" disabled />
                <label for="customerdataa_name">Nome</label>
                <input type="text" id="customerdata_surname" name="surname" value="<?=$user["surname"]?>" disabled />
                <label for="customerdata_surname">Cognome</label>
                <input type="email" id="customerdata_registrationemail" name="email" value="<?=$user["email"]?>" disabled />
                <label for="customerdata_registrationemail">Email</label>
                <input type="date" id="customerdata_birthdate" name="birthdate" value="<?=$user["birthdate"]?>" disabled />
                <label for="customerdata_birthdate">Data di nascita</label>
                <input type="text" id="customerdata_address" name="address" value="<?=$user["address"]?>" disabled/>
                <label for="customerdata_address">Indirizzo di residenza</label>
                <input type="text" id="customerdata_city" name="city" value="<?=$user["city"]?>" disabled/>
                <label for="customerdata_city">Città</label>
                <input type="text" id="customerdata_zipcode" name="zipcode" value="<?=$user["zipcode"]?>" disabled/>
                <label for="customerdata_zipcode">CAP</label>
                <input type="text" id="customerdata_country" name="country" value="<?=$user["country"]?>" disabled/>
                <label for="customerdata_country">Provincia</label>
                <input type="date" id="customerdata_registrationdate" name="registrationdate" value="<?=$user["registration_date"]?>" disabled />
                <label for="customerdata_registrationdate">Data di Registrazione</label>
            </form>
        </section>
        <?php
    break;
    case "admin":
        $user = fetch(query("SELECT u.name, u.surname, u.email FROM users u WHERE u.id = '".escape($_GET["id"])."'"));
        ?>
        <section id="admindata" class="margintop">
            <h1>Dati dell'Admin</h1>
            <form>
                <input type="text" id="admindataname" name="name" value="<?=$user["name"]?>" disabled />
                <label for="admindataname">Nome</label>
                <input type="text" id="admindatasurname" name="surname" value="<?=$user["surname"]?>" disabled />
                <label for="admindatasurname">Cognome</label>
                <input type="email" id="admindataemail" name="email" value="<?=$user["email"]?>" disabled />
                <label for="admindatasurname">Email</label>
            </form>
        </section>
        <?php
    break;
}
include(__DIR__."/inc/footer.php");
?>
